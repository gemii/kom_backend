package cc.gemii.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;
import com.alibaba.fastjson.JSON;

import cc.gemii.component.JedisClient;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.QuestionCustom;
import cc.gemii.pojo.TempMessage;
import cc.gemii.util.HttpClientUtil;

public class KOMTest {

	@Test
	public void HTTPTest(){
		String url = "http://localhost:8080/weixin/wx/getUserinfoByFW";
		Map<String, String> param = new HashMap<String, String>();
		param.put("openid", "oNPcuvwLFMGdvWTqxKJwKBrEwKV4");
		String json = HttpClientUtil.doPost(url, param);
		System.out.println(json);
	}
	
	@Test
	public void resultTest(){
		QuestionCustom qc = new QuestionCustom();
		qc.setDescription("test");
		CommonResult cr = CommonResult.ok(qc);
		String json = JSON.toJSONString(cr);
		Map<String, Object> cr2 = (Map<String, Object>)JSON.parse(json);
		Map<String, Object> data = (Map<String, Object>)cr2.get("data");
		System.out.println(data.get("description"));
	}
	
	public String getAccesstoken(){
		String appid = "wxdf1e1e7e53868799";
		String secret = "20a2827999d4850095ed76a76f2f90dc";
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET".replace("APPID", appid).replace("APPSECRET", secret);
		String json = HttpClientUtil.doGet(url);
		Map<String, Object> map = (Map<String, Object>)JSON.parse(json);
		return map.get("access_token").toString();
	}
	
	@Test
	public void setHangye(){
		String url = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=" + this.getAccesstoken();
		Map<String, String> map = new HashMap<String, String>();
		map.put("industry_id1", "1");
		map.put("industry_id2", "2");
		System.out.println(HttpClientUtil.doPostJson(url, JSON.toJSONString(map)));
	}
	
	@Test
	public void messageTest(){
		String url = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=" + this.getAccesstoken();
		System.out.println(HttpClientUtil.doGet(url));
	}
	
	@Test
	public void gethangye(){
		String url = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=" + "BRxC1dBVbwJyOgAynlLlxBoBlPoEH4ZP_CjPjcwr4_X2BaSERITLEZeltbxDH7KwZMNSlkrW3UtEU_8EgmR3Z7SIA8MO4gQdwbcepu880_mGstMmD7XRboVyJTuAv2mWBFBcACAMQM";
		System.out.println(HttpClientUtil.doGet(url));
	}
	
	@Test
	public void getTempId(){
		String url = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=" + "BRxC1dBVbwJyOgAynlLlxBoBlPoEH4ZP_CjPjcwr4_X2BaSERITLEZeltbxDH7KwZMNSlkrW3UtEU_8EgmR3Z7SIA8MO4gQdwbcepu880_mGstMmD7XRboVyJTuAv2mWBFBcACAMQM";
		Map<String, String> map = new HashMap<String, String>();
		map.put("template_id_short", "TM00200");
		System.out.println(HttpClientUtil.doPost(url, map));
	}
	
	@Test
	public void sendMessageTest(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=5Tr5F8UHHbsxFtYrExyeW9uZ4ZYzR2PCiavvly2lkHGDAUUay70XmtgbQZ2lvC_nC5Fdh8LFjNxihv0lLFS4N5Iq7FIn5uvjRQzYttfAXjsBZShACAGEX";
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("touser", "oNPcuvwLFMGdvWTqxKJwKBrEwKV4");
		response.put("template_id", "7MjWa5J4ep-2vRAE_lF81sOKkAgRnBsKOO9wTdqGKr8");
		response.put("url", "http://www.baidu.com");
		Map<String, Object> data = new HashMap<String, Object>();
		TempMessage first = new TempMessage();
		first.setValue("新回答通知");
		first.setColor("#000000");
		data.put("first", first);
		
		TempMessage CreateDate = new TempMessage();
		CreateDate.setColor("#000000");
		CreateDate.setValue(sdf.format(new Date()));
		data.put("CreateDate", CreateDate);
		
		TempMessage ProcessingResults = new TempMessage();
		ProcessingResults.setColor("#000000");
		ProcessingResults.setValue("您的问题:有了新的回答");
		data.put("ProcessingResults", ProcessingResults);
		
		TempMessage remark = new TempMessage();
		remark.setColor("#000000");
		remark.setValue("请点击查看");
		data.put("remark", remark);
		
		response.put("data", data);
		HttpClientUtil.doPostJson(url, JSON.toJSONString(response));
	}
	
	@Test
	public void jedisTest(){
		//创建一个spring容器
		ApplicationContext ac = new ClassPathXmlApplicationContext("classpath:spring/applicationContext-*.xml");
		JedisClient jedisClient = ac.getBean(JedisClient.class);
		System.out.println(jedisClient.get("blogsCount"));
	}
	
	@Test
	public void subscribe_test(){
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxIdle(5);
		config.setMaxTotal(500);
		config.setMaxWaitMillis(100000000);
		config.setTestOnBorrow(false);
		JedisPool jedisPool = new JedisPool(config,"115.159.212.238",6379);
		Jedis jedis = jedisPool.getResource();
		jedis.auth("jinzili373690");
		jedis.subscribe(new JedisPubSub() {

			@Override
			public void onMessage(String channel, String message) {
				System.out.println("收到频道 : 【" + channel +" 】的消息 ：" + message); 
			}

			@Override
			public void onPMessage(String pattern, String channel,
					String message) {
				// TODO Auto-generated method stub
				super.onPMessage(pattern, channel, message);
			}

			@Override
			public void onPSubscribe(String pattern, int subscribedChannels) {
				// TODO Auto-generated method stub
				super.onPSubscribe(pattern, subscribedChannels);
			}

			@Override
			public void onPUnsubscribe(String pattern, int subscribedChannels) {
				// TODO Auto-generated method stub
				super.onPUnsubscribe(pattern, subscribedChannels);
			}

			@Override
			public void onSubscribe(String channel, int number) {
				System.out.println("channel on: "+channel); 
                System.out.println("number :"+number); 
			}

			@Override
			public void onUnsubscribe(String channel, int number) {
				// TODO Auto-generated method stub
				System.out.println("channel onun:" + channel);
				System.out.println("number:" + number);
			}
			
		}, "channel1","channel2");
		jedisPool.destroy();
	}
	
	@Test
	public void publish_test(){
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxIdle(5);
		config.setMaxTotal(500);
		config.setMaxWaitMillis(100000000);
		config.setTestOnBorrow(false);
		JedisPool jedisPool = new JedisPool(config,"115.159.212.238",6379);
		Jedis jedis = jedisPool.getResource();
		jedis.auth("jinzili373690");
		 long i = jedis.publish("channel1", "channel1的朋友们，你们好吗？亲"); 
        System.out.println(i+" 个订阅者接受到了 channel1 消息"); 
        i = jedis.publish("channel2", "你好呀，亲"); 
        System.out.println(i+" 个订阅者接受到了 channel2 消息"); 
        jedisPool.returnResource(jedis); 
	}
}
