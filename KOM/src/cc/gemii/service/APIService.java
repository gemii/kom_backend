package cc.gemii.service;

import java.io.File;

import cc.gemii.pojo.CommonResult;

public interface APIService {

	/**
	 * 
	 * @param end 结束时间   格式  yyyy-MM-dd HH:mm:ss
	 * @param start 开始时间   格式  yyyy-MM-dd HH:mm:ss
	 * @return
	 * TODO 获取回答者回答数
	 */
	CommonResult getAnswers(String start, String end);

	/**
	 * 
	 * @return
	 * TODO 获取回答者建议
	 */
	CommonResult getSuggestion();

	/**
	 * 
	 * @return
	 * TODO 从NPLUS接口接受kom信息
	 */
	CommonResult insertKom();

	/**
	 * 
	 * @param start 开始时间
	 * @param end_string 结束时间
	 * @param filepath 文件路径
	 * @return
	 * TODO
	 */
	File getFile(String start, String end_string, String filepath);

}
