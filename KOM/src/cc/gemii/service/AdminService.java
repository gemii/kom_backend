package cc.gemii.service;

import cc.gemii.pojo.CommonResult;

public interface AdminService {

	/**
	 * 
	 * @param page 页码
	 * @return
	 * TODO 根据页码获取问题列表
	 */
	CommonResult getQuestionsByPage(Integer page);

	/**
	 * 
	 * @param id 问题id
	 * @return
	 * TODO 根据id删除问题
	 */
	CommonResult deleteQuestion(Integer id);

	/**
	 * 
	 * @param id 回答id
	 * @return
	 * TODO 根据id删除回答
	 */
	CommonResult deleteAnswer(Integer id);

	/**
	 * 
	 * @return 
	 * TODO
	 */
	CommonResult getTotal();

	/**
	 * 
	 * @param password 用户输入的密码
	 * @return
	 * TODO
	 */
	CommonResult validte(String password);

}
