package cc.gemii.service;


import cc.gemii.po.NotRemind;
import cc.gemii.po.Question;
import cc.gemii.po.QuestionerSug;
import cc.gemii.pojo.CommonResult;

public interface QuestionService {

	

	/**
	 * 
	 * @param question 表单接收
	 * @param unionid 用户唯一标识
	 * @param openid 用户openid
	 * @return
	 * TODO
	 */
	CommonResult insertQuestion(Question question, String unionid, String openid);

	/**
	 * 
	 * @param openid 用户openid
	 * @param page 当前页码
	 * @return
	 * TODO 根据页码获取此用户发表过的问题
	 */
	CommonResult getMyQuestions(String openid, Integer page);

	/**
	 * 
	 * @param qid 问题id
	 * @return
	 * TODO
	 */
	CommonResult getQuestionById(Integer qid);

	/**
	 * 
	 * @param openid 用户openid
	 * @param page 当前页码
	 * @return
	 * TODO 根据页码获取此用户关注过的问题
	 */
	CommonResult getMyAttention(String openid, Integer page);

	/**
	 * 
	 * @param qs 建议包装类
	 * @param openid 用户openid
	 * @param unionid 用户唯一标识
	 * @return
	 * TODO 提问者提交建议
	 */
	CommonResult insertSuggestion(QuestionerSug qs, String openid,
			String unionid);

	/**
	 * 
	 * @param openid 用户openid
	 * @param unionid 用户unionid
	 * @param qid 问题id
	 * @return
	 * TODO 用户添加关注
	 */
	CommonResult insertAttention(String openid, String unionid, Integer qid);

	/**
	 * 
	 * @param openid 用户openid
	 * @param qid 要取消关注的问题id
	 * @return
	 * TODO
	 */
	CommonResult deleteAttention(String openid, Integer id);

	/**
	 * 
	 * @param openid 用户openid
	 * @param unionid 用户唯一标识
	 * @param notRemind 不再提醒包装类
	 * @return
	 * TODO 用户点击不再提醒
	 */
	CommonResult notRemind(String openid, String unionid, NotRemind notRemind);

	/**
	 * 
	 * @param openid 用户openid
	 * @param page 页码
	 * @param timestamp 第一次加载时间戳
	 * @param last_id 最后一条的id
	 * @return
	 * TODO
	 */
	CommonResult getQuestionList(String openid, Integer page, String timestamp, Integer last_id);

}
