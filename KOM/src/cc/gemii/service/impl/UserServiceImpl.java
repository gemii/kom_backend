package cc.gemii.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.KomMapper;
import cc.gemii.po.Kom;
import cc.gemii.service.JudgeService;
import cc.gemii.service.UserService;
import cc.gemii.util.HttpClientUtil;

import com.alibaba.fastjson.JSON;

@Service
public class UserServiceImpl implements UserService{

	@Value("${GETUSER_URL}")
	private String GETUSER_URL;
	
	@Autowired
	private KomMapper komMapper;
	
	@Autowired
	private JudgeService judgeService;
	
	@Override
	public Map<String, Object> getUserinfo(String code) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", code);
		Map<String, Object> result = (Map<String, Object>)JSON.parse(HttpClientUtil.doPost(GETUSER_URL, map));
		Map<String, Object> userAllInfo = (Map<String, Object>)result.get("data");
		Map<String, Object> user = new HashMap<String, Object>();
		user.put("unionid", userAllInfo.get("unionid").toString());
		
		String openid = userAllInfo.get("fwOpenid").toString();
		
		user.put("openid", openid);
		user.put("nickname", userAllInfo.get("nickname").toString());
		user.put("headimgurl", userAllInfo.get("headimgurl").toString());
		
		Kom kom = komMapper.selectByPrimaryKey(openid);
		if(kom == null){
			user.put("groupNickname", null);
		}else{
			user.put("groupNickname", kom.getGroupNickname());
		}
		
		user.put("isBan", judgeService.isBanned(openid));
		return user;
	}

}
