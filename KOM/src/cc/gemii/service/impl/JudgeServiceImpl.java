package cc.gemii.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.BannedMapper;
import cc.gemii.po.Banned;
import cc.gemii.po.BannedExample;
import cc.gemii.service.JudgeService;
@Service
public class JudgeServiceImpl implements JudgeService{

	@Autowired
	private BannedMapper bMapper;
	
	@Override
	public Banned isBanned(String openid) {
		BannedExample example = new BannedExample();
		example.createCriteria()
			.andOpenidEqualTo(openid);
		List<Banned> list = bMapper.selectByExample(example);
		if(list.size() == 0){
			return null;
		}else{
			Banned ban = list.get(0);
			Date now = new Date();
			if(now.after(ban.getEndTime())){
				return null;
			}
			return ban;
		}
	}

}
