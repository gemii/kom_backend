package cc.gemii.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.AnswerCustomMapper;
import cc.gemii.mapper.AnswerMapper;
import cc.gemii.mapper.AnswererSugMapper;
import cc.gemii.mapper.KomGroupMapper;
import cc.gemii.mapper.KomMapper;
import cc.gemii.mapper.QuestionCustomMapper;
import cc.gemii.po.AnswererSug;
import cc.gemii.po.AnswererSugExample;
import cc.gemii.po.Kom;
import cc.gemii.po.KomGroup;
import cc.gemii.po.KomGroupExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.Record;
import cc.gemii.service.APIService;
import cc.gemii.util.ExportExcel;
import cc.gemii.util.HttpClientUtil;

import com.alibaba.fastjson.JSON;
@Service
public class APIServiceImpl implements APIService {

	@Autowired
	private AnswerMapper answerMapper;
	
	@Autowired
	private AnswerCustomMapper acMapper;
	
	@Autowired
	private AnswererSugMapper asMapper;
	
	@Autowired
	private KomMapper komMapper;
	
	@Autowired
	private KomGroupMapper komGroupMapper;
	
	@Autowired
	private QuestionCustomMapper qcMapper;
	
	@Value("${GETKOM_INFO_URL}")
	private String GETKOM_INFO_URL;
	
	@Override
	public CommonResult getAnswers(String start,String end) {
		try {
			if(start == null || "".equals(start) || end == null || "".equals(end)){
				start = "2016-09-01";
				end = "2116-09-26";
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date start_date = sdf.parse(start);
			Date end_date = sdf.parse(end);
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(end_date);
			calendar.add(Calendar.DATE, 1);
			List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
			Map<String, String> infoMap = new HashMap<String, String>();
			infoMap.put("start", start);
			infoMap.put("end", sdf.format(calendar.getTime()));
			List<String> openids = acMapper.getAnswersOpenid();
			for (String openid : openids) {
				Map<String, Object> map = new HashMap<String, Object>();
				infoMap.put("openid", openid);
				map.put("openid", openid);
				map.put("effective", acMapper.getEffectiveByOpenid(infoMap));
				map.put("deleted", acMapper.getDeletedByOpenid(infoMap));
				list.add(map);
			}
			return CommonResult.ok(list);
		} catch (Exception e) {
			return CommonResult.build(-1, "请求失败!请注意时间格式");
		}
	}

	@Override
	public CommonResult getSuggestion() {
		AnswererSugExample asExample = new AnswererSugExample();
		asExample.setOrderByClause("status");
		List<AnswererSug> list = asMapper.selectByExampleWithBLOBs(asExample);
		for (AnswererSug answererSug : list) {
			if(answererSug.getStatus() == 0){
				answererSug.setStatus(1);
				asMapper.updateByPrimaryKeySelective(answererSug);
				answererSug.setStatus(0);
			}
		}
		return CommonResult.ok(list);
	}

	@Override
	public CommonResult insertKom() {
		Map<String, Object> kom_info_map = (Map<String, Object>)JSON.parse(HttpClientUtil.doGet(GETKOM_INFO_URL));
		
		if(kom_info_map.get("status").equals("true")){
			List<Map<String, Object>> each_info = (List<Map<String, Object>>)kom_info_map.get("data");
			for (Map<String, Object> each_map : each_info) {
				String openid = String.valueOf(each_map.get("OpenId"));
				if(openid == null || "".equals(openid) || "null".equals(openid)){
					continue;
				}
				Kom kom = new Kom();
				kom.setBeginDate(String.valueOf(each_map.get("BeginDate")));
				kom.setCity(String.valueOf(each_map.get("City")));
				kom.setCreated(new Date());
				kom.setGroupNickname(String.valueOf(each_map.get("GroupName")));
				kom.setGroupStatus(String.valueOf(each_map.get("GroupStatus")));
				kom.setHospital(String.valueOf(each_map.get("Hospital")));
				kom.setMobile(String.valueOf(each_map.get("Mobile")));
				kom.setName(String.valueOf(each_map.get("Name")));
				kom.setOpenid(openid);
				kom.setProvince(String.valueOf(each_map.get("Province")));
				kom.setRecruitmentChannels(String.valueOf(each_map.get("RecruitmentChannels")));
				kom.setUniqueid(String.valueOf(each_map.get("UniqueID")));
				kom.setWeixinNickname(String.valueOf(each_map.get("WeixinNickname")));
				kom.setWyethid(String.valueOf(each_map.get("WyethID")));
				
				if(komMapper.selectByPrimaryKey(openid) == null){
					komMapper.insert(kom);
				}else{
					komMapper.updateByPrimaryKeySelective(kom);
				}
				
				String groupName = String.valueOf(each_map.get("RoomName"));
				KomGroupExample kgExample = new KomGroupExample();
				kgExample.createCriteria()
					.andGroupNameEqualTo(groupName)
					.andOpenidEqualTo(openid);
				
				if(komGroupMapper.selectByExample(kgExample).size() == 0){
					KomGroup kg = new KomGroup();
					kg.setGroupName(groupName);
					kg.setOpenid(openid);
					komGroupMapper.insert(kg);
				}
			}
			return CommonResult.ok();
		}else{
			return CommonResult.build(500, "请求数据失败");
		}
	}

	@Override
	public File getFile(String start, String end, String filepath) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Map<String, String> map = null;
			Date start_date = sdf.parse(start);
			Date end_date = sdf.parse(end);
			List<Record> total = new ArrayList<Record>();
			
			//查询总记录
			start = sdf2.format(start_date);
			end = sdf2.format(end_date);
			map = new HashMap<String, String>();
			map.put("start", start);
			map.put("end", end);
			Record totalRecord = qcMapper.getRecord(map);
			totalRecord.setStart(start);
			totalRecord.setEnd(end);
			
			Calendar c = Calendar.getInstance();
			c.setTime(start_date);
			c.add(Calendar.DATE, 1);
			while(c.getTime().before(end_date)){
				start = sdf2.format(start_date);
				end = sdf2.format(c.getTime());
				map = new HashMap<String, String>();
				map.put("start", start);
				map.put("end", end);
				Record record = qcMapper.getRecord(map);
				record.setStart(start);
				record.setEnd(end);
				total.add(record);
				
				start_date = c.getTime();
				c.add(Calendar.DATE, 1);
			}
			
			start = sdf2.format(start_date);
			end = sdf2.format(end_date);
			map = new HashMap<String, String>();
			map.put("start", start);
			map.put("end", end);
			Record record = qcMapper.getRecord(map);
			record.setStart(start);
			record.setEnd(end);
			total.add(record);
			
			//添加总记录
			total.add(totalRecord);
			
			ExportExcel<Record> export = new ExportExcel<Record>();
			OutputStream out = new BufferedOutputStream(new FileOutputStream(filepath));
			String title = "record";
			String[] headers = {"开始时间","结束时间","真实妈妈提问","新回答","没有被回答的问题个数","回答用户数"};
			export.exportExcel(title,headers, total, out);  //title是excel表中底部显示的表格名，如Sheet
			out.close();
			return new File(filepath);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
