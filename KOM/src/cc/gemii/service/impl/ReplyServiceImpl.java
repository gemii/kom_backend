package cc.gemii.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cc.gemii.component.JedisClient;
import cc.gemii.mapper.AnswerCustomMapper;
import cc.gemii.mapper.AnswerMapper;
import cc.gemii.mapper.AnswererSugMapper;
import cc.gemii.mapper.KomMapper;
import cc.gemii.mapper.QuestionCustomMapper;
import cc.gemii.mapper.QuestionMapper;
import cc.gemii.po.Answer;
import cc.gemii.po.AnswerExample;
import cc.gemii.po.AnswererSug;
import cc.gemii.po.Question;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.QuestionCustom;
import cc.gemii.pojo.TempMessage;
import cc.gemii.service.ReplyService;
import cc.gemii.util.HttpClientUtil;
@Service
public class ReplyServiceImpl implements ReplyService {

	@Autowired
	private AnswerMapper answerMapper;
	
	@Autowired
	private AnswerCustomMapper acMapper;
	
	@Autowired
	private QuestionMapper questionMapper;
	
	@Autowired
	private QuestionCustomMapper qcMapper;
	
	@Autowired
	private AnswererSugMapper asMapper;
	
	@Autowired
	private KomMapper komMapper;
	
	@Autowired
	private JedisClient jedisClient;
	
	@Value("${ANSWERER_SIZE}")
	private Integer ANSWERER_SIZE;
	
	@Value("${GET_ACCESSTOKEN_FW}")
	private String GET_ACCESSTOKEN_FW;
	
	@Value("${QUESTIONER_SYSTEM}")
	private String QUESTIONER_SYSTEM;
	
	@Override
	public CommonResult getMyAnswer(String openid, Integer page) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", (page - 1) * ANSWERER_SIZE);
		map.put("pagesize", ANSWERER_SIZE);
		map.put("openid", openid);
		List<Question> list = acMapper.getMyAnswer(map);
		return CommonResult.ok(list);
	}

	@Override
	public CommonResult getQuestionById(String openid, Integer qid) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("question", questionMapper.selectByPrimaryKey(qid));
		
		//取得自己的回答
		AnswerExample ownExample = new AnswerExample();
		ownExample.createCriteria()
			.andQidEqualTo(qid)
			.andOpenidEqualTo(openid)
			.andStatusEqualTo(0);
		;
		map.put("ownAnswers", answerMapper.selectByExampleWithBLOBs(ownExample));
		
		//取的其他人的回答
		AnswerExample othersExample = new AnswerExample();
		othersExample.createCriteria()
			.andQidEqualTo(qid)
			.andOpenidNotEqualTo(openid)
			.andStatusEqualTo(0);
		map.put("othersAnswer", answerMapper.selectByExampleWithBLOBs(othersExample));
		return CommonResult.ok(map);
	}

	@Override
	public CommonResult updateAnswer(Answer answer, Integer answerId) {
		answer.setCreated(new Date());
		answerMapper.updateByPrimaryKeySelective(answer);
		Question question = questionMapper.selectByPrimaryKey(answerMapper.selectByPrimaryKey(answerId).getQid());
		question.setUpdated(new Date());
		questionMapper.updateByPrimaryKeySelective(question);
		this.sendMessageToUser(question.getId(), question.getTitle());
		return CommonResult.ok();
	}

	@Override
	public CommonResult insertSuggestion(AnswererSug answererSug,
			String openid, String unionid) {
		answererSug.setCreated(new Date());
		answererSug.setOpenid(openid);
		answererSug.setUnionid(unionid);
		answererSug.setStatus(0);
		asMapper.insert(answererSug);
		return CommonResult.ok();
	}

	@Override
	public CommonResult insertAnswer(String openid, String unionid,
			Answer answer,Integer qid,String groupNickname) {
		
		answer.setOpenid(openid);
		answer.setUnionid(unionid);
		answer.setCreated(new Date());
		answer.setStatus(0);
		answer.setGroupNickname(groupNickname);
		//更新该问题的回答数
		qcMapper.updateQuestionCount(qid);
		answerMapper.insert(answer);
		
		//向关注此问题的用户发送模板消息
		
		this.sendMessageToUser(qid,questionMapper.selectByPrimaryKey(qid).getTitle());
		return CommonResult.ok();
	}

	@Override
	public CommonResult getQuestionList(String openid, Integer page,
			String timestamp, Integer last_id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("openid", openid);
		map.put("timestamp", timestamp);
		map.put("last_id", last_id);
		map.put("start", (page - 1) * ANSWERER_SIZE);
		map.put("pagesize", ANSWERER_SIZE);
		List<QuestionCustom> list = acMapper.getQuestionList(map);
		return CommonResult.ok(list);
	}
	
	private void sendMessageToUser(int qid,String title){
		List<String> openids = acMapper.getOpenidByQid(qid);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String access_token = HttpClientUtil.doGet(GET_ACCESSTOKEN_FW);
		for (String openid : openids) {
			String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + access_token;
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("touser", openid);
			response.put("template_id", "7MjWa5J4ep-2vRAE_lF81sOKkAgRnBsKOO9wTdqGKr8");
			response.put("url", QUESTIONER_SYSTEM);
			Map<String, Object> data = new HashMap<String, Object>();
			TempMessage first = new TempMessage();
			first.setValue("新回答通知");
			first.setColor("#000000");
			data.put("first", first);
			
			TempMessage CreateDate = new TempMessage();
			CreateDate.setColor("#000000");
			CreateDate.setValue(sdf.format(new Date()));
			data.put("CreateDate", CreateDate);
			
			TempMessage ProcessingResults = new TempMessage();
			ProcessingResults.setColor("#000000");
			ProcessingResults.setValue("您的问题[" + title + "]有了新的回答");
			data.put("ProcessingResults", ProcessingResults);
			
			TempMessage remark = new TempMessage();
			remark.setColor("#000000");
			remark.setValue("请点击查看");
			data.put("remark", remark);
			
			response.put("data", data);
			HttpClientUtil.doPostJson(url, JSON.toJSONString(response));
		}
	}
}
