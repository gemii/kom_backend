package cc.gemii.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cc.gemii.mapper.AttentionMapper;
import cc.gemii.mapper.KomFansMapper;
import cc.gemii.mapper.NotRemindMapper;
import cc.gemii.po.Attention;
import cc.gemii.po.KomFans;
import cc.gemii.po.NotRemind;
import cc.gemii.po.NotRemindExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.CommonService;
import cc.gemii.util.HttpClientUtil;
@Service
public class CommonServiceImpl implements CommonService {

	@Autowired
	private AttentionMapper attentionMapper;
	
	@Autowired
	private NotRemindMapper nrMapper;
	
	@Autowired
	private KomFansMapper kfMapper;
	
	@Value("${GETUSERINFO_FW}")
	private String getUserinfoURL;
	
	@Override
	public CommonResult insertFollow(String openid, String unionid, Integer qid,Integer number) {
		Attention attention = new Attention();
		attention.setCreated(new Date());
		attention.setOpenid(openid);
		attention.setUnionid(unionid);
		attention.setQid(qid);
		attentionMapper.insert(attention);
		
		
		return CommonResult.ok(this.isRemind(openid, number));
	}

	@Override
	public boolean isRemind(String openid, Integer num) {
		KomFans fan = kfMapper.selectByPrimaryKey(openid);
		if(fan == null){  //如果此用户不是服务号的粉丝
			Map<String,String> map = new HashMap<String, String>();
			map.put("openid", openid);
			String json = HttpClientUtil.doPost(getUserinfoURL, map);
			Map<String, Object> userinfo = (Map<String, Object>)JSON.parse(json);
			if(userinfo.get("subscribe").equals(1)){  //用户关注了服务号,不再提醒
				KomFans newFan = new KomFans();
				newFan.setOpenid(openid);
				newFan.setCreated(new Date());
				kfMapper.insert(newFan);
				return false;
			}
			
			NotRemindExample nrExample = new NotRemindExample();
			nrExample.createCriteria()
				.andOpenidEqualTo(openid)
				.andNumberEqualTo(num);
			List<NotRemind> list = nrMapper.selectByExample(nrExample);
			if(list.size() > 0){   //用户这个位置点击过不再提醒
				return false;
			}
			//其他情况
			return true;
		}else{   //用户在粉丝表中
			return false;
		}
	}
}
