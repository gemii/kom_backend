package cc.gemii.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.AnswerCustomMapper;
import cc.gemii.mapper.AnswerMapper;
import cc.gemii.mapper.AttentionMapper;
import cc.gemii.mapper.KomMapper;
import cc.gemii.mapper.NotRemindMapper;
import cc.gemii.mapper.QuestionCustomMapper;
import cc.gemii.mapper.QuestionMapper;
import cc.gemii.mapper.QuestionerSugMapper;
import cc.gemii.po.Attention;
import cc.gemii.po.AttentionExample;
import cc.gemii.po.Kom;
import cc.gemii.po.NotRemind;
import cc.gemii.po.Question;
import cc.gemii.po.QuestionerSug;
import cc.gemii.pojo.AnswerCustom;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.QuestionCustom;
import cc.gemii.service.CommonService;
import cc.gemii.service.QuestionService;
@Service
public class QuestionServiceImpl implements QuestionService{

	@Autowired
	private QuestionCustomMapper qcMapper;
	
	@Autowired
	private QuestionMapper qMapper;
	
	@Autowired
	private AnswerMapper answerMapper;
	
	@Autowired
	private AnswerCustomMapper acMapper;
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private QuestionerSugMapper qsMapper;
	
	@Autowired
	private AttentionMapper attentionMapper;
	
	@Autowired
	private NotRemindMapper nrMapper;
	
	@Autowired
	private KomMapper komMapper;
	
	@Value("${INSERT_Q_NUM}")
	private Integer INSERT_Q_NUM;

	@Value("${INSERT_S_NUM}")
	private Integer INSERT_S_NUM;
	
	@Value("${INSERT_A_NUM}")
	private Integer INSERT_A_NUM;
	
	@Value("${QUESTIONER_SIZE}")
	private Integer questioner_size;
	
	@Override
	public CommonResult insertQuestion(Question question, String unionid,
			String openid) {
		question.setCount(0);
		question.setCreated(new Date());
		question.setOpenid(openid);
		question.setUnionid(unionid);
		question.setStatus(0);
		question.setUpdated(new Date());
		qMapper.insert(question);
		return CommonResult.ok(commonService.isRemind(openid, INSERT_Q_NUM));
	}

	@Override
	public CommonResult getQuestionById(Integer qid) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		QuestionCustom question = qcMapper.getQuestionById(qid);
		map.put("question", question);
		
		List<AnswerCustom> acList = acMapper.getAnswersByQid(qid);
		for (AnswerCustom ac : acList) {
			Kom kom = komMapper.selectByPrimaryKey(ac.getOpenid());
			ac.setNickname(kom.getGroupNickname());
		}
		map.put("answers", acList);
		return CommonResult.ok(map);
	}

	@Override
	public CommonResult getMyQuestions(String openid,Integer page) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("openid", openid);
		map.put("start", (page - 1) * questioner_size);
		map.put("pagesize", questioner_size);
		List<QuestionCustom> list = qcMapper.getMyQuestions(map);
		return CommonResult.ok(list);
	}
	
	@Override
	public CommonResult getMyAttention(String openid, Integer page) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("openid", openid);
		map.put("start", (page - 1) * questioner_size);
		map.put("pagesize", questioner_size);
		List<QuestionCustom> list = qcMapper.getMyAttention(map);
		return CommonResult.ok(list);
	}

	@Override
	public CommonResult insertSuggestion(QuestionerSug qs, String openid,
			String unionid) {
		qs.setOpenid(openid);
		qs.setUnionid(unionid);
		qs.setCreated(new Date());
		qsMapper.insert(qs);
		return CommonResult.ok(commonService.isRemind(openid, INSERT_S_NUM));
	}

	@Override
	public CommonResult insertAttention(String openid, String unionid,
			Integer qid) {
		AttentionExample aExample = new AttentionExample();
		aExample.createCriteria()
			.andOpenidEqualTo(openid)
			.andQidEqualTo(qid);
		List<Attention> list = attentionMapper.selectByExample(aExample);
		if(list.size() == 0){
			Attention attention = new Attention();
			attention.setCreated(new Date());
			attention.setOpenid(openid);
			attention.setUnionid(unionid);
			attention.setQid(qid);
			attention.setStatus(0);
			attentionMapper.insert(attention);
		}else{
			Attention attention = list.get(0);
			attention.setStatus(0);
			attentionMapper.updateByPrimaryKeySelective(attention);
		}
		return CommonResult.ok(commonService.isRemind(openid, INSERT_A_NUM));
	}

	@Override
	public CommonResult deleteAttention(String openid, Integer qid) {
		AttentionExample aExample = new AttentionExample();
		aExample.createCriteria()
			.andOpenidEqualTo(openid)
			.andQidEqualTo(qid);
		Attention attention = new Attention();
		attention.setStatus(1);
		attentionMapper.updateByExampleSelective(attention, aExample);
		return CommonResult.ok();
	}

	@Override
	public CommonResult notRemind(String openid, String unionid, NotRemind notRemind) {
		notRemind.setCreated(new Date());
		notRemind.setOpenid(openid);
		notRemind.setUnionid(unionid);
		nrMapper.insert(notRemind);
		return CommonResult.ok();
	}

	@Override
	public CommonResult getQuestionList(String openid,Integer page, String timestamp,
			Integer last_id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("openid", openid);
		map.put("timestamp", timestamp);
		map.put("last_id", last_id);
		map.put("start", (page - 1) * questioner_size);
		map.put("pagesize", questioner_size);
		List<QuestionCustom> list = qcMapper.getQuestionList(map);
		return CommonResult.ok(list);
	}
}
