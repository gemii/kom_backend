package cc.gemii.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.AnswerCustomMapper;
import cc.gemii.mapper.AnswerMapper;
import cc.gemii.mapper.QuestionCustomMapper;
import cc.gemii.po.Answer;
import cc.gemii.po.AnswerExample;
import cc.gemii.po.Question;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.AdminService;
import cc.gemii.util.MD5Utils;
@Service
public class AdminServiceImpl implements AdminService{

	@Value("${ADMIN_SIZE}")
	private Integer admin_size;
	
	@Value("${PASSWORD}")
	private String password;
	
	@Autowired
	private QuestionCustomMapper qcMapper;
	
	@Autowired
	private AnswerMapper answerMapper;
	
	@Autowired
	private AnswerCustomMapper acMapper;
	@Override
	public CommonResult getQuestionsByPage(Integer page) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("start", (page - 1) * admin_size);
		map.put("pagesize", admin_size);
		List<Question> list = qcMapper.getQuestionsByPage(map);
		
		List<Map<String, Object>> responseList = new ArrayList<Map<String,Object>>();
		for (Question question : list) {   //遍历问题列表,取出每个问题的答案,并包装
			AnswerExample aExample = new AnswerExample();
			aExample.createCriteria()
				.andQidEqualTo(question.getId())   //条件：问题id
				.andStatusEqualTo(0);			   //条件：状态为0，未被删除
			//根据条件取出此问题的答案
			List<Answer> answers = answerMapper.selectByExampleWithBLOBs(aExample);
			
			Map<String, Object> each = new HashMap<String, Object>();
			each.put("question", question);
			each.put("answers", answers);
			responseList.add(each);
		}
		return CommonResult.ok(responseList);
	}

	
	
	@Override
	public CommonResult deleteQuestion(Integer id) {
		qcMapper.deleteQuestion(id);
		acMapper.deleteAnswersByQid(id);
		return CommonResult.ok();
	}

	@Override
	public CommonResult deleteAnswer(Integer id) {
		acMapper.deleteAnswerById(id);
		qcMapper.updateCountByDelA(answerMapper.selectByPrimaryKey(id).getQid());
		return CommonResult.ok();
	}

	@Override
	public CommonResult getTotal() {
		return CommonResult.ok(qcMapper.getTotal());
	}

	@Override
	public CommonResult validte(String pwd) {
		if(MD5Utils.md5Encode(pwd).equals(password)){
			return CommonResult.ok();
		}
		return CommonResult.build(-1, "密码错误");
	}

}
