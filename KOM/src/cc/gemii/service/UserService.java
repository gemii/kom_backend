package cc.gemii.service;

import java.util.Map;

public interface UserService {

	Map<String, Object> getUserinfo(String code);

}
