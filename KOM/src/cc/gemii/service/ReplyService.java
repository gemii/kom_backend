package cc.gemii.service;

import cc.gemii.po.Answer;
import cc.gemii.po.AnswererSug;
import cc.gemii.pojo.CommonResult;

public interface ReplyService {

	/**
	 * 
	 * @param openid 用户openid
	 * @param page 当前页码
	 * @return 根据页码获取回答列表
	 * TODO 回答者查看我的回答
	 */
	CommonResult getMyAnswer(String openid, Integer page);

	/**
	 * 
	 * @param openid 用户openid
	 * @param qid 问题id
	 * @return
	 * TODO 回答者系统中获取问题详细信息,此用户答案排在答案列表最前方
	 */
	CommonResult getQuestionById(String openid, Integer qid);

	/**
	 * 
	 * @param answer 答案包装类
	 * @param answerId 答案id
	 * @return 回答者更新自己的答案
	 * TODO
	 */
	CommonResult updateAnswer(Answer answer, Integer answerId);

	/**
	 * 
	 * @param answererSug 回答者建议包装类
	 * @param openid 用户openid
	 * @param unionid 用户唯一标识
	 * @return 
	 * TODO 回答者系统中回答者提交建议
	 */
	CommonResult insertSuggestion(AnswererSug answererSug, String openid,
			String unionid);

	/**
	 * 
	 * @param openid 用户openid
	 * @param unionid 用户unionid
	 * @param answer 答案包装类
	 * @param qid 此问题id
	 * @param groupNickname  回答者群内昵称
	 * @return
	 * TODO 回答者提交答案
	 */
	CommonResult insertAnswer(String openid, String unionid, Answer answer, Integer qid, String groupNickname);

	/**
	 * 
	 * @param openid 用户openid
	 * @param page 页码
	 * @param timestamp 第一次加载时间戳
	 * @param last_id 最后一条id
	 * @return
	 * TODO
	 */
	CommonResult getQuestionList(String openid, Integer page, String timestamp,
			Integer last_id);
}
