package cc.gemii.service;

import cc.gemii.pojo.CommonResult;

public interface CommonService {

	/**
	 * 
	 * @param openid 用户openid
	 * @param unionid 用户unionid
	 * @param qid 问题id
	 * @param number 提醒位置编号
	 * @return
	 * TODO
	 */
	CommonResult insertFollow(String openid, String unionid, Integer qid, Integer number);

	/**
	 * 
	 * @param openid 用户openid
	 * @param num 提示位置编号    提问者：  问题列表 1    提问  2     个人中心客服   3
	 * @return
	 * TODO
	 */
	boolean isRemind(String openid,Integer num);
}
