package cc.gemii.service;

import org.springframework.stereotype.Service;

import cc.gemii.po.Banned;

@Service
public interface JudgeService {

	/**
	 * 
	 * @param openid 用户openid
	 * @return
	 * TODO 根据用户openid判断是否被封禁 0未封禁   1封禁
	 */
	Banned isBanned(String openid);

}
