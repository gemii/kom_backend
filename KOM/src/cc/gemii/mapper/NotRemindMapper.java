package cc.gemii.mapper;

import cc.gemii.po.NotRemind;
import cc.gemii.po.NotRemindExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NotRemindMapper {
    int countByExample(NotRemindExample example);

    int deleteByExample(NotRemindExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(NotRemind record);

    int insertSelective(NotRemind record);

    List<NotRemind> selectByExample(NotRemindExample example);

    NotRemind selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") NotRemind record, @Param("example") NotRemindExample example);

    int updateByExample(@Param("record") NotRemind record, @Param("example") NotRemindExample example);

    int updateByPrimaryKeySelective(NotRemind record);

    int updateByPrimaryKey(NotRemind record);
}