package cc.gemii.mapper;

import cc.gemii.po.AnswererSug;
import cc.gemii.po.AnswererSugExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AnswererSugMapper {
    int countByExample(AnswererSugExample example);

    int deleteByExample(AnswererSugExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(AnswererSug record);

    int insertSelective(AnswererSug record);

    List<AnswererSug> selectByExampleWithBLOBs(AnswererSugExample example);

    List<AnswererSug> selectByExample(AnswererSugExample example);

    AnswererSug selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") AnswererSug record, @Param("example") AnswererSugExample example);

    int updateByExampleWithBLOBs(@Param("record") AnswererSug record, @Param("example") AnswererSugExample example);

    int updateByExample(@Param("record") AnswererSug record, @Param("example") AnswererSugExample example);

    int updateByPrimaryKeySelective(AnswererSug record);

    int updateByPrimaryKeyWithBLOBs(AnswererSug record);

    int updateByPrimaryKey(AnswererSug record);
}