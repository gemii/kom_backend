package cc.gemii.mapper;

import cc.gemii.po.KomFans;
import cc.gemii.po.KomFansExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface KomFansMapper {
    int countByExample(KomFansExample example);

    int deleteByExample(KomFansExample example);

    int deleteByPrimaryKey(String openid);

    int insert(KomFans record);

    int insertSelective(KomFans record);

    List<KomFans> selectByExample(KomFansExample example);

    KomFans selectByPrimaryKey(String openid);

    int updateByExampleSelective(@Param("record") KomFans record, @Param("example") KomFansExample example);

    int updateByExample(@Param("record") KomFans record, @Param("example") KomFansExample example);

    int updateByPrimaryKeySelective(KomFans record);

    int updateByPrimaryKey(KomFans record);
}