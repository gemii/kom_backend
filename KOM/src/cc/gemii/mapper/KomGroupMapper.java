package cc.gemii.mapper;

import cc.gemii.po.KomGroup;
import cc.gemii.po.KomGroupExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface KomGroupMapper {
    int countByExample(KomGroupExample example);

    int deleteByExample(KomGroupExample example);

    int insert(KomGroup record);

    int insertSelective(KomGroup record);

    List<KomGroup> selectByExample(KomGroupExample example);

    int updateByExampleSelective(@Param("record") KomGroup record, @Param("example") KomGroupExample example);

    int updateByExample(@Param("record") KomGroup record, @Param("example") KomGroupExample example);
}