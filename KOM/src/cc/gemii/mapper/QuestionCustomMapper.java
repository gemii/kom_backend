package cc.gemii.mapper;

import cc.gemii.po.Question;
import cc.gemii.pojo.QuestionCustom;
import cc.gemii.pojo.Record;

import java.util.List;
import java.util.Map;

public interface QuestionCustomMapper {
	List<QuestionCustom> getMyQuestions(Map<String, Object> map);

	List<QuestionCustom> getMyAttention(Map<String, Object> map);
	
	void updateQuestionCount(int id);
	
	QuestionCustom getQuestionById(int id);
	
	List<QuestionCustom> getQuestionList(Map<String, Object> map);
	
	List<Question> getQuestionsByPage(Map<String, Integer> map);

	void deleteQuestion(Integer id);
	
	int getTotal();

	void updateCountByDelA(int qid);
	
	Record getRecord(Map<String, String> map);
}