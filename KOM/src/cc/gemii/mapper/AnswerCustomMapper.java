package cc.gemii.mapper;

import cc.gemii.po.Question;
import cc.gemii.pojo.AnswerCustom;
import cc.gemii.pojo.QuestionCustom;

import java.util.List;
import java.util.Map;

public interface AnswerCustomMapper {
    List<Question> getMyAnswer(Map<String, Object> map);
    
    List<String> getAnswersOpenid();
    
    Integer getEffectiveByOpenid(Map<String, String> map);
    
    Integer getDeletedByOpenid(Map<String, String> map);

    Integer getCauseDeletedByOpenid(Map<String, String> map);

	void deleteAnswersByQid(Integer id);

	void deleteAnswerById(Integer id);

	List<QuestionCustom> getQuestionList(Map<String, Object> map);

	List<AnswerCustom> getAnswersByQid(Integer qid);
	
	List<String> getOpenidByQid(Integer qid);
}