package cc.gemii.mapper;

import cc.gemii.po.QuestionerSug;
import cc.gemii.po.QuestionerSugExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface QuestionerSugMapper {
    int countByExample(QuestionerSugExample example);

    int deleteByExample(QuestionerSugExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(QuestionerSug record);

    int insertSelective(QuestionerSug record);

    List<QuestionerSug> selectByExampleWithBLOBs(QuestionerSugExample example);

    List<QuestionerSug> selectByExample(QuestionerSugExample example);

    QuestionerSug selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") QuestionerSug record, @Param("example") QuestionerSugExample example);

    int updateByExampleWithBLOBs(@Param("record") QuestionerSug record, @Param("example") QuestionerSugExample example);

    int updateByExample(@Param("record") QuestionerSug record, @Param("example") QuestionerSugExample example);

    int updateByPrimaryKeySelective(QuestionerSug record);

    int updateByPrimaryKeyWithBLOBs(QuestionerSug record);

    int updateByPrimaryKey(QuestionerSug record);
}