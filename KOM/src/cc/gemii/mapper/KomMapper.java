package cc.gemii.mapper;

import cc.gemii.po.Kom;
import cc.gemii.po.KomExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface KomMapper {
    int countByExample(KomExample example);

    int deleteByExample(KomExample example);

    int deleteByPrimaryKey(String openid);

    int insert(Kom record);

    int insertSelective(Kom record);

    List<Kom> selectByExample(KomExample example);

    Kom selectByPrimaryKey(String openid);

    int updateByExampleSelective(@Param("record") Kom record, @Param("example") KomExample example);

    int updateByExample(@Param("record") Kom record, @Param("example") KomExample example);

    int updateByPrimaryKeySelective(Kom record);

    int updateByPrimaryKey(Kom record);
}