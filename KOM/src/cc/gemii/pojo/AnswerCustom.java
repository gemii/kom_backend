package cc.gemii.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

import cc.gemii.po.Answer;

public class AnswerCustom extends Answer{

	private String createdString;

	
	private String nickname;
	
	
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getCreatedString() {
		return createdString;
	}

	public void setCreatedString(String createdString) {
		this.createdString = createdString;
	}

	@Override
	public void setCreated(Date created) {
		super.setCreated(created);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.setCreatedString(sdf.format(created));
	}
}
