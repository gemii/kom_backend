package cc.gemii.pojo;

public class Record {

	private String start;
	
	private String end;
	
	private Integer question;
	
	private Integer answer;
	
	private Integer withoutAnswer;

	private Integer user;
	
	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public Integer getQuestion() {
		return question;
	}

	public void setQuestion(Integer question) {
		this.question = question;
	}

	public Integer getAnswer() {
		return answer;
	}

	public void setAnswer(Integer answer) {
		this.answer = answer;
	}

	public Integer getWithoutAnswer() {
		return withoutAnswer;
	}

	public void setWithoutAnswer(Integer withoutAnswer) {
		this.withoutAnswer = withoutAnswer;
	}

	public Integer getUser() {
		return user;
	}

	public void setUser(Integer user) {
		this.user = user;
	}
	
}
