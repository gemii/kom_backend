package cc.gemii.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

import cc.gemii.po.Question;

public class QuestionCustom extends Question {

	private Integer isAttention;

	private String createdString;
	
	public Integer getIsAttention() {
		return isAttention;
	}

	public void setIsAttention(Integer isAttention) {
		this.isAttention = isAttention;
	}

	public String getCreatedString() {
		return createdString;
	}

	public void setCreatedString(String createdString) {
		this.createdString = createdString;
	}

	@Override
	public void setCreated(Date created) {
		// TODO Auto-generated method stub
		super.setCreated(created);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.setCreatedString(sdf.format(created));
	}
	
}
