package cc.gemii.po;

import java.util.Date;

public class Kom {
    private String openid;

    private String groupNickname;

    private String weixinNickname;

    private String beginDate;

    private String hospital;

    private String groupStatus;

    private String uniqueid;

    private String recruitmentChannels;

    private String province;

    private String city;

    private String name;

    private String wyethid;

    private String mobile;

    private Date created;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public String getGroupNickname() {
        return groupNickname;
    }

    public void setGroupNickname(String groupNickname) {
        this.groupNickname = groupNickname == null ? null : groupNickname.trim();
    }

    public String getWeixinNickname() {
        return weixinNickname;
    }

    public void setWeixinNickname(String weixinNickname) {
        this.weixinNickname = weixinNickname == null ? null : weixinNickname.trim();
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate == null ? null : beginDate.trim();
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital == null ? null : hospital.trim();
    }

    public String getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(String groupStatus) {
        this.groupStatus = groupStatus == null ? null : groupStatus.trim();
    }

    public String getUniqueid() {
        return uniqueid;
    }

    public void setUniqueid(String uniqueid) {
        this.uniqueid = uniqueid == null ? null : uniqueid.trim();
    }

    public String getRecruitmentChannels() {
        return recruitmentChannels;
    }

    public void setRecruitmentChannels(String recruitmentChannels) {
        this.recruitmentChannels = recruitmentChannels == null ? null : recruitmentChannels.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getWyethid() {
        return wyethid;
    }

    public void setWyethid(String wyethid) {
        this.wyethid = wyethid == null ? null : wyethid.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setGroupName(String name){
    	this.groupNickname=name;
    }
	@Override
	public String toString() {
		return "Kom [openid=" + openid + ", groupNickname=" + groupNickname
				+ ", weixinNickname=" + weixinNickname + ", beginDate="
				+ beginDate + ", hospital=" + hospital + ", groupStatus="
				+ groupStatus + ", uniqueid=" + uniqueid
				+ ", recruitmentChannels=" + recruitmentChannels
				+ ", province=" + province + ", city=" + city + ", name="
				+ name + ", wyethid=" + wyethid + ", mobile=" + mobile
				+ ", created=" + created + "]";
	}
    
    
}