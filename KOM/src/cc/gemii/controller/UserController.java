package cc.gemii.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	
	
	@RequestMapping("/getUserinfoByQ")
	@ResponseBody
	public CommonResult getUserinfoByQ(@RequestParam String code,HttpSession session){
		Map<String, Object> user = this.userService.getUserinfo(code);
		session.setAttribute("openid", user.get("openid"));
		session.setAttribute("unionid", user.get("unionid"));
		session.setAttribute("headimgurl", user.get("headimgurl"));
		return CommonResult.ok(user);
	}
	
	@RequestMapping("/getUserinfoByA")
	@ResponseBody
	public CommonResult getUserinfoByA(@RequestParam String code,HttpSession session){
		Map<String, Object> user = this.userService.getUserinfo(code);
		session.setAttribute("openid", user.get("openid"));
		session.setAttribute("unionid", user.get("unionid"));
		session.setAttribute("headimgurl", user.get("headimgurl"));
		session.setAttribute("groupNickname",user.get("groupNickname"));
		return CommonResult.ok(user);
	}
}
