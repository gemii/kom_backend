package cc.gemii.controller;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.APIService;

@Controller
@RequestMapping("/api")
public class APIController {

	@Autowired
	private APIService apiService;
	
	@RequestMapping("/getAnswers")
	@ResponseBody
	public CommonResult getAnswers(@RequestParam(required=false) String start,@RequestParam(required=false) String end){
		return apiService.getAnswers(start,end);
	}
	
	@RequestMapping("/getSuggestion")
	@ResponseBody
	public CommonResult getSuggestion(){
		return apiService.getSuggestion();
	}
	
	@RequestMapping("/insertKom")
	@ResponseBody
	public CommonResult insertKom(){
		return apiService.insertKom();
	}
	
	@RequestMapping("/getRecord")  
	public ResponseEntity<byte[]> download(@RequestParam(required=false) String start,
			@RequestParam(required=false) String end,
			HttpServletRequest request) {
		try {
			HttpHeaders headers = new HttpHeaders();  
		    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		    
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		    Date start_time = sdf.parse(start);
		    String end_string = "";
		    if(end == null || "".equals(end)){
		    	end_string = sdf.format(new Date());
		    }else{
		    	Date end_time = sdf.parse(end);
		    	end_string = end;
		    }
		    String path = request.getSession().getServletContext().getRealPath("") + "/file/";
		    String filename = "KOM_" + start + "-" + end_string + ".xls";
		    headers.setContentDispositionFormData("attachment", URLEncoder.encode(filename,"UTF-8")); 
		    return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(apiService.getFile(start,end_string,path + filename)),  headers, HttpStatus.CREATED);  
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	    
	}
	
	public static void main(String[] args) throws Exception {
		String start = "2016-10-01 12:00:01";
		String end = "2016-11-01 17:00:01";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date start_date = sdf.parse(start);
		Date end_date = sdf.parse(end);
		Calendar c = Calendar.getInstance();
		c.setTime(start_date);
		c.add(Calendar.DATE, 1);
		while(c.getTime().before(end_date)){
			System.out.println(sdf.format(c.getTime()));
			c.add(Calendar.DATE, 1);
		}
	}
}
