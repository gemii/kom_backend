package cc.gemii.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.NotRemind;
import cc.gemii.po.Question;
import cc.gemii.po.QuestionerSug;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.QuestionService;

@Controller
@RequestMapping("/question")
public class QuestionController {

	@Autowired
	private QuestionService questionService;
	
	@RequestMapping("/getQuestions")
	@ResponseBody
	public CommonResult getAllQuestions(@RequestParam Integer page,HttpSession session,Integer last_id,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		if(page == 1){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			session.setAttribute("timestamp", sdf.format(new Date()));
		}
		return questionService.getQuestionList(
				(String) session.getAttribute("openid"),
				page,
				(String) session.getAttribute("timestamp"),last_id);
	}
	
	@RequestMapping("/insertQuestion")
	@ResponseBody
	public CommonResult insertQuestion(Question question,HttpSession session,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return questionService.insertQuestion(question,
				(String) session.getAttribute("unionid"),
				(String) session.getAttribute("openid"));
	}
	
	@RequestMapping("/getMyQuestions")
	@ResponseBody
	public CommonResult getMyQuestions(HttpSession session,@RequestParam Integer page ,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return questionService.getMyQuestions(
				(String) session.getAttribute("openid"),
				page);
	}
	
	@RequestMapping("/getMyAttention")
	@ResponseBody
	public CommonResult getMyAttention(HttpSession session,@RequestParam Integer page,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return questionService.getMyAttention(
				(String) session.getAttribute("openid"),
				page);
	}
	
	@RequestMapping("/getQuestionById")
	@ResponseBody
	public CommonResult getQuestionById(@RequestParam Integer id,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return questionService.getQuestionById(id);
	}
	
	@RequestMapping("/insertSuggestion")
	@ResponseBody
	public CommonResult insertSuggestion(QuestionerSug qs,HttpSession session,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return questionService.insertSuggestion(qs,
				(String) session.getAttribute("openid"),
				(String) session.getAttribute("unionid"));
	}
	
	@RequestMapping("/insertAttention")
	@ResponseBody
	public CommonResult insertAttention(HttpSession session,Integer id,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return questionService.insertAttention(
				(String) session.getAttribute("openid"),
				(String) session.getAttribute("unionid"),
				id);
	}
	
	@RequestMapping("/deleteAttention")
	@ResponseBody
	public CommonResult deleteAttention(HttpSession session,Integer id,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return questionService.deleteAttention(
				(String) session.getAttribute("openid"),
				id);
	}
	
	@RequestMapping("/notRemind")
	@ResponseBody
	public CommonResult notRemind(HttpSession session,NotRemind notRemind,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return questionService.notRemind(
				(String) session.getAttribute("openid"),
				(String) session.getAttribute("unionid"),
				notRemind);
	}
	
	@RequestMapping("/test")
	@ResponseBody
	public String test(HttpSession session){
		session.setAttribute("openid", "oNPcuvwLFMGdvWTqxKJwKBrEwKV4");
		session.setAttribute("unionid", "unionid");
		return "success";
	}
}
