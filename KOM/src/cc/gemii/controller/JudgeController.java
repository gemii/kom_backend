package cc.gemii.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Banned;
import cc.gemii.service.JudgeService;

@Controller
@RequestMapping("/judge")
public class JudgeController {

	@Autowired
	private JudgeService judgeService;
	
	@RequestMapping("/isBanned")
	@ResponseBody
	public Banned isBanned(HttpSession session){
		String openid = (String) session.getAttribute("openid");
		return judgeService.isBanned(openid);
	}
}
