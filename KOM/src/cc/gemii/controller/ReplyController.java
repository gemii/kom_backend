package cc.gemii.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Answer;
import cc.gemii.po.AnswererSug;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.ReplyService;

@Controller
@RequestMapping("/reply")
public class ReplyController {

	@Autowired
	private ReplyService replyService;
	
	@RequestMapping("/getQuestions")
	@ResponseBody
	public CommonResult getAllQuestions(@RequestParam Integer page,HttpSession session,Integer last_id,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		if(page == 1){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			session.setAttribute("timestamp", sdf.format(new Date()));
		}
		return replyService.getQuestionList((String) session.getAttribute("openid"),
				page,
				(String) session.getAttribute("timestamp"),last_id);
	}
	
	@RequestMapping("/getMyAnswer")
	@ResponseBody
	public CommonResult getMyAnswer(HttpSession session,
			@RequestParam Integer page,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return replyService.getMyAnswer((String) session.getAttribute("openid"),page);
	}
	
	@RequestMapping("/getQuestionById")
	@ResponseBody
	public CommonResult getQuestionById(HttpSession session,Integer id ,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return replyService.getQuestionById((String) session.getAttribute("openid"),id);
	}
	
	@RequestMapping("/updateAnswer")
	@ResponseBody
	public CommonResult updateAnswer(Answer answer,@RequestParam Integer id,HttpServletResponse response){
		return replyService.updateAnswer(answer,id);
	}
	
	@RequestMapping("/insertSuggestion")
	@ResponseBody
	public CommonResult insertSuggestion(AnswererSug answererSug,HttpSession session,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return replyService.insertSuggestion(answererSug,
				(String) session.getAttribute("openid"),
				(String) session.getAttribute("unionid"));
	}
	
	@RequestMapping("/insertAnswer")
	@ResponseBody
	public CommonResult insertAnswer(Answer answer,HttpSession session,@RequestParam Integer qid,HttpServletResponse response){
		return replyService.insertAnswer(
				(String) session.getAttribute("openid"),
				(String) session.getAttribute("unionid"),
				answer,
				qid,
				(String) session.getAttribute("groupNickname"));
	}
}
