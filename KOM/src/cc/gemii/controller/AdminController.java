package cc.gemii.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.AdminService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private AdminService adminService;
	
	@RequestMapping("/getQuestions")
	@ResponseBody
	public CommonResult getQuestions(@RequestParam Integer page){
		return adminService.getQuestionsByPage(page);
	}
	
	@RequestMapping("/deleteQuestion")
	@ResponseBody
	public CommonResult deleteQuestion(@RequestParam Integer id){
		return adminService.deleteQuestion(id);
	}
	
	@RequestMapping("/deleteAnswer")
	@ResponseBody
	public CommonResult deleteAnswer(@RequestParam Integer id){
		return adminService.deleteAnswer(id);
	}
	
	@RequestMapping("/getTotal")
	@ResponseBody
	public CommonResult getTotal(){
		return adminService.getTotal();
	}
	
	@RequestMapping("/validate")
	@ResponseBody
	public CommonResult validate(@RequestParam String password){
		return adminService.validte(password);
	}
}
